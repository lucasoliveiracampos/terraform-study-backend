cp ../consumer/target/rabbitmq-consumer*.jar ../consumer/docker
cp ../producer/target/rabbitmq-producer*.jar ../producer/docker
docker network create rabbitmq_network
docker-compose build
docker-compose up
docker run --rm -d -p 5672:5672 -p 15672:15672 --hostname my-rabbit --name some-rabbit rabbitmq:3.11.2-management
