package com.dreams.terraformstudybackend.primenumbers.service;

import com.dreams.terraformstudybackend.primenumbers.repository.PrimeNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrimeNumberService {

//    @Autowired
//    public PrimeNumberRepository primeNumberRepository;
//
    public Integer calculateQuantityOfPrimeNumbersUntil(final Integer number) {
        int quantity = 0;
        for (int i = 1; i <= number; i++) {
            if (isPrime(i)) {
                quantity++;
            }
        }
//        primeNumberRepository.save(
//                new PrimeNumber(number, quantity)
//        );
        return quantity;
    }

    public boolean isPrime(int n) {
        int count = 0;

        // 0, 1 negative numbers are not prime
        if (n < 2) return false;

        // checking the number of divisors b/w 1 and the number n-1
        for (int i = 2; i < n; i++) {
            if (n % i == 0) return false;
        }

        // if reached here then must be true
        return true;
    }
}
