package com.dreams.terraformstudybackend.habbitmq;

import com.dreams.terraformstudybackend.primenumbers.service.PrimeNumberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class Consumer {

    final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    PrimeNumberService primeNumberService;

    private CountDownLatch latch = new CountDownLatch(1);

    @RabbitListener(queues = {"${queue.quantity-prime-numbers-until}"})
    public void receiveMessage(@Payload String message) throws InterruptedException {
        logger.info("Received request to calculate prime numbers until: <" + message + ">");
        logger.info("Calculating...");
        Thread.sleep(250);
        int result = primeNumberService.calculateQuantityOfPrimeNumbersUntil(Integer.valueOf(message));
        logger.info("result is: " + result);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
