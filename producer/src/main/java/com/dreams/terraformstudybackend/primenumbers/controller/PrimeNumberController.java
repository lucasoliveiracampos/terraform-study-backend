package com.dreams.terraformstudybackend.primenumbers.controller;


import com.dreams.terraformstudybackend.primenumbers.service.PrimeNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/prime-number")
public class PrimeNumberController {

    @Autowired
    PrimeNumberService primeNumberService;

    @PostMapping(value="/calculate-quantity-until")
    public Integer calculateQuantityOfPrimeNumbersUntil(@RequestParam(required = true) final Integer number) {
        return primeNumberService.calculateQuantityOfPrimeNumbersUntil(number);
    }

}