package com.dreams.terraformstudybackend.primenumbers.repository;

import com.dreams.terraformstudybackend.primenumbers.model.PrimeNumber;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PrimeNumberRepository extends PagingAndSortingRepository<PrimeNumber, Long> {

}
