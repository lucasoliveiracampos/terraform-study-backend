package com.dreams.terraformstudybackend.primenumbers.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PrimeNumber {

    @Id
    public Long id;
    public Integer untilNumber;
    public Integer quantity;

    public PrimeNumber(final Integer untilNumber, final Integer quantity) {
        this.id = System.currentTimeMillis();
        this.untilNumber = untilNumber;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUntilNumber() {
        return untilNumber;
    }

    public void setUntilNumber(Integer untilNumber) {
        this.untilNumber = untilNumber;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
