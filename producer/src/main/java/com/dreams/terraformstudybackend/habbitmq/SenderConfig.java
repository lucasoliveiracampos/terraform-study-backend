package com.dreams.terraformstudybackend.habbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@Configuration
public class SenderConfig {

    @Value("${queue.quantity-prime-numbers-until}")
    private String message;

    @Bean
    public Queue queue() {
        return new Queue(message, true);
    }
}
