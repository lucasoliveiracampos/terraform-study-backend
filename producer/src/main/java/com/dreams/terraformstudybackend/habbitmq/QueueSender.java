package com.dreams.terraformstudybackend.habbitmq;

import org.springframework.amqp.AmqpIOException;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.SocketException;
import java.util.Random;

@Component
@EnableAsync
@EnableScheduling
public class QueueSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Queue queue;

    private Random rand = new Random();


    public void send(String order) {
        rabbitTemplate.convertAndSend(this.queue.getName(), order);
    }

    @Async
    @Scheduled(fixedRate = 25)
    @Retryable(
            value = {SocketException.class, AmqpIOException.class},
            maxAttempts = 5,
            backoff = @Backoff(multiplier = 2, delay = 3000)
    )
    public void queueCalculateQuantityOfPrimeNumbersUntil() {
        send(String.valueOf(rand.nextInt(1, 1000)));
    }
}
